<?php
namespace controllers{

	class Queue extends Config {

		public function getAll($queue) {
		    global $app;
		    
            $this->opts['http']['content'] = json_encode([
                "QueueAlias" => $queue,
                "LockItems" =>  0,
                "LockSpan" => null,
                "Attempts" => 10,
                "Page" => [
                    "PageIndex" => 0,
                    "PageSize" => 20
                ],
                "Where" => "",
                "WhereMetadata" => "",
                "OrderBy" => ""
            ]);
            $context  = stream_context_create($this->opts);
            
            $result = file_get_contents($this->url . '/v1/Queue/API.svc/web/SearchQueueItems', false, $context);
            $app->render('default.php', ["data" => json_decode($result)], 200);
		}
		
		public function dequeueItem($arrayQueueItems) {
		    global $app;
		    
            $this->opts['http']['content'] = json_encode([
                "QueueItems" => $arrayQueueItems
            ]);
            $context  = stream_context_create($this->opts);
            
            $result = file_get_contents($this->url . '/v1/Queue/API.svc/web/DequeueQueueItems', false, $context);
            $app->render('default.php', ["data" => json_decode($result)], 200);
		}

	}
}