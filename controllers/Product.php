<?php
namespace controllers{

	class Product extends Config {

		public function get($id) {
		    global $app;

            $postdata = $id;
            $this->opts['http']['content'] = $id;
            
            $context  = stream_context_create($this->opts);
            
            $result = file_get_contents($this->url . '/v1/Catalog/API.svc/web/GetProduct', false, $context);
            
            $app->render('default.php', ["data" => json_decode($result)], 200);
            
            if (self::LOG_ENABLED) :
                Log::write('product_' . date('Ymd') . '.log', $id);
            endif;
		}

	}
}