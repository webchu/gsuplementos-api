<?php
namespace controllers{

	class Pedido extends Config {

		public function get($orderNumber) {
		    
            $this->opts['http']['content'] = '"' . $orderNumber .'"';
            $context  = stream_context_create($this->opts);
            
            $result = file_get_contents($this->url . '/v1/Sales/API.svc/web/GetOrderByNumber', false, $context);
            return json_decode($result);
		}
	}
}