<?php
namespace controllers{

	class Fila extends Config {

		public function getAll($queue) {
            $this->opts['http']['content'] = json_encode([
                "QueueAlias" => $queue,
                "LockItems" =>  0,
                "LockSpan" => null,
                "Attempts" => 10,
                "Page" => [
                    "PageIndex" => 0,
                    "PageSize" => 2
                ],
                "Where" => "",
                "WhereMetadata" => "",
                "OrderBy" => ""
            ]);
            $context  = stream_context_create($this->opts);
            
            $result = file_get_contents($this->url . '/v1/Queue/API.svc/web/SearchQueueItems', false, $context);
            return json_decode($result);
		}
		
		public function dequeueItem($arrayQueueItems) {
            $this->opts['http']['content'] = json_encode([
                "QueueItems" => $arrayQueueItems
            ]);
            $context  = stream_context_create($this->opts);
            
            $result = file_get_contents($this->url . '/v1/Queue/API.svc/web/DequeueQueueItems', false, $context);
            return json_decode($result);
		}

	}
}