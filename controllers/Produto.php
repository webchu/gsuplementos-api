<?php
namespace controllers{
    
	class Produto extends Config {
	    
		public function get($id) {
            $this->opts['http']['content'] = $id;
            $context  = stream_context_create($this->opts);
            $result = file_get_contents($this->url . '/v1/Catalog/API.svc/web/GetProduct', false, $context);
            
            if (self::LOG_ENABLED) :
                Log::write('product_' . date('Ymd') . '.log', $id);
            endif;
            
            return json_decode($result);
		}
		
		public function getAll($where = '') {
            $this->opts['http']['content'] = json_encode([
                "Page" => [
                    "PageIndex" => 0,
                    "PageSize" => 5
                ],
                "Where" => $where,
                "WhereMetadata" => "",
                "OrderBy" => ""
            ]);
            $context = stream_context_create($this->opts);
            $result = file_get_contents($this->url . '/v1/Catalog/API.svc/web/SearchProduct', false, $context);
            
            if (self::LOG_ENABLED) :
                //Log::write('products_' . date('Ymd') . '.log', $id);
            endif;
            
            return json_decode($result);
		}
	}
}