<?php
namespace controllers{

	class Sales extends Config {

		public function get($orderNumber) {
		    global $app;
		    
            $this->opts['http']['content'] = '"' . $orderNumber .'"';
            $context  = stream_context_create($this->opts);
            
            $result = file_get_contents($this->url . '/v1/Sales/API.svc/web/GetOrderByNumber', false, $context);
            $app->render('default.php', ["data" => json_decode($result)], 200);
		}

	}
}