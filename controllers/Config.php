<?php
namespace controllers{

	abstract class Config {
	    protected $opts;
        protected $url = 'http://layer.gsuplementos.corecommerce.com.br';
        
        const LOG_ENABLED = true;
	    
	    function __construct() {
	        $this->opts = array('http' =>
                array(
                    'header' => array( 
                        //'Authorization: Basic Y29yZS5hdGl2YWNhbzpjMHImQHQxdjk0OA==',
                        'Authorization: Basic YXBpOmFwaQ==',
                        'Accept: application/json', 
			            'Content-Type: application/json'
                    ),
                    'method'  => 'POST'
                )
            );
	    }
	}
}