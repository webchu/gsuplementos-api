<?php
//header('Content-Type: application/json');

//Autoload
$loader = require 'vendor/autoload.php';

//Instanciando objeto
$app = new \Slim\Slim(array(
    'templates.path' => 'templates'
));

//Listando todos os pedidos
$app->get('/orders/', function() use ($app){
	(new \controllers\Sales($app))->orders();
});

//get order
$app->get('/order/:id', function($id) use ($app){
	(new \controllers\Sales($app))->get($id);
});

/**
 * Filas 
 */
// carrega todos os itens da fila
$app->get('/queue/:id', function($id) use ($app){
	(new \controllers\Queue($app))->getAll($id);
});

// Retirando o item consumido da fila
$app->get('/dequeue-item/:id', function($id) use ($app){
	(new \controllers\Queue($app))->dequeueItem($id);
});



//get produto
$app->get('/product/:id', function($id) use ($app){
	(new \controllers\Product($app))->get($id);
});


//Rodando aplicação
$app->run();