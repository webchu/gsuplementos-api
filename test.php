<?php

//Autoload
$loader = require 'vendor/autoload.php';

// carregando informações de um produto
echo '<h1>Carregando informações de um produto</h1>';
$id = '985936';

$product = (new \controllers\Produto())->get($id);
echo sprintf('%s - %s', $product->ProductID, $product->Name);

echo '<hr />';

// listando produtos
echo '<h1>Listando 5 produtos</h1>';
$products = (new \controllers\Produto())->getAll();
if ($products->IsValid && $products->Page->PageSize > 0) :
	$p = new \controllers\Produto();
	foreach ($products->Result as $item) :
		echo sprintf('%s - %s<br />', $item->ProductID, $item->Name);
	endforeach;
endif;

// carrega itens da fila
echo '<h1>Carrega itens da fila</h1>';
$queueName = 'novo-programa';
$q = new \controllers\Fila();
$items = $q->getAll($queueName);

if ($items->IsValid &&  $items->Page->PageSize > 0):
	echo sprintf('Nome da da fila: %s<br />', $queueName);
	echo sprintf('Total de itens na fila: %s<br />', $items->Page->RecordCount);
	
	$p = new \controllers\Pedido();
	foreach ($items->Result as $item) :
		$arrayQueueItems[] = $item->QueueItemID;
		$pedido = $p->get($item->EntityKeyValue);
		
		echo '<hr />';
		
		echo sprintf('Pedido %s<br />', $pedido->OrderNumber);
		echo sprintf('Total %s<br />', $pedido->Total);
	endforeach;	
	
	echo '<hr />';
	echo ('<h1>Retira itens da fila</h1>');
	echo '<pre>' . print_r($arrayQueueItems, 1) . '</pre>';
	//$ret = $q->dequeueItem($arrayQueueItems);
	//print_r($ret);
endif;


